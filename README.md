# You will be able to

1. Explain what **object orientation** is and why it's used
2. **Create a class** with attributes and methods and **create instances** of that class
3. **Use reader and writer methods** to access and modify data

# Object Orientation

<img src="http://curriculum-content.s3.amazonaws.com/KWK/oo-intro-dog.png" alt="white dog with lists of dog properties and dog behaviors" height="200" align="right" />

**Object orientation is one of the most powerful concepts in programming**, and today that power will become yours. Nearly everything in Ruby is an object. What exactly is an object, though? An object is a programming construct comprised of attributes and actions. Its attributes are its data, and its actions are its methods. Take the string `"popcorn"`, for instance. It's an object with a length attribute of 7. That is, you can describe it as having 7 characters in it. It also has actions, or methods like `reverse` and `upcase`. In this lesson, you'll learn about how to create 'blueprints' (known as **classes**), which allow us to build Ruby objects modeled after real world systems and relationships. Ever wonder how Facebook can have over one billion users, or how a video game can have hundreds of detailed characters? The answer is object orientation.
